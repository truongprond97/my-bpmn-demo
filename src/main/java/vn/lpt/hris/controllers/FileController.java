package vn.lpt.hris.controllers;

import lombok.SneakyThrows;
import org.drools.decisiontable.InputType;
import org.drools.decisiontable.SpreadsheetCompiler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import vn.lpt.hris.config.DroolsConfiguration;
import vn.lpt.hris.domains.ProcessRulePath;
import vn.lpt.hris.domains.ReloadDroolsRules;
import vn.lpt.hris.models.ProcessRulePathDTO;
import vn.lpt.hris.models.UploadFileResponse;
import vn.lpt.hris.repositories.ProcessRulePathRepository;
import vn.lpt.hris.services.FileStorageService;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/rules")
public class FileController {
    private static final Logger logger = LoggerFactory.getLogger(FileController.class);
//    @Resource
    private ReloadDroolsRules rules;
    private final ProcessRulePathRepository processRulePathRepository;

//    @Autowired
    private final FileStorageService fileStorageService;

    public FileController(ReloadDroolsRules rules, FileStorageService fileStorageService,ProcessRulePathRepository processRulePathRepository) {
        this.rules = rules;
        this.fileStorageService = fileStorageService;
        this.processRulePathRepository = processRulePathRepository;
    }

        @PostMapping("/applyRuleToProcess")
    public ProcessRulePathDTO applyRuleToProcess(@RequestParam("file") MultipartFile file,
                                                 @RequestParam("processKey") String processKey) {
        String fileName = fileStorageService.storeFile(file);

        ProcessRulePath processRulePath = new ProcessRulePath("C:/Users/Admin/OneDrive/Desktop/hris-bpm (2)/hris-bpm"+DroolsConfiguration.FULL_PATH+fileName,processKey);
//        ProcessRulePath processRulePath = new ProcessRulePath("C://Users/Admin/OneDrive/Desktop/DiscountRule2.xls",processKey);
        ProcessRulePath newProcessRulePath =  processRulePathRepository.save(processRulePath);

//        InputStream is = null;
//        try {
//            is= new FileInputStream("C:\\Users\\Admin\\OneDrive\\Desktop\\"+fileName);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//        SpreadsheetCompiler sc = new SpreadsheetCompiler();
//        String rules=sc.compile(is, InputType.XLS);
//        StringBuffer drl = new StringBuffer(rules);
//        System.out.println("drool file == " + drl);

//
//        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
//                .path("/resources/rules/")
//                .path(fileName)
//                .toUriString();
//
//        return new UploadFileResponse(fileName, fileDownloadUri,
//                file.getContentType(), file.getSize());
        return new ProcessRulePathDTO(newProcessRulePath);
    }

    @PostMapping("/uploadFile")
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file) {
        String fileName = fileStorageService.storeFile(file);


        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/resources/rules/")
                .path(fileName)
                .toUriString();

        return new UploadFileResponse(fileName, fileDownloadUri,
                file.getContentType(), file.getSize());
    }

    @PostMapping("/uploadMultipleFiles")
    public List<UploadFileResponse> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
        return Arrays.asList(files)
                .stream()
                .map(file -> uploadFile(file))
                .collect(Collectors.toList());
    }

    @GetMapping("/downloadFile/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = fileStorageService.loadFileAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
}
