package vn.lpt.hris.controllers;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.drools.decisiontable.InputType;
import org.drools.decisiontable.SpreadsheetCompiler;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.internal.utils.KieHelper;
import org.springframework.web.bind.annotation.*;
import vn.lpt.hris.domains.DRRequest;

import vn.lpt.hris.models.OrderDetailDTO;
import vn.lpt.hris.models.ProcessRulePathDTO;
import vn.lpt.hris.repositories.OrderDetailRepository;

import vn.lpt.hris.services.ProcessService;
import vn.lpt.hris.services.impl.ProcessRulePathServiceImpl;
import vn.lpt.hris.services.impl.ProcessServiceImpl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/test")
@RequiredArgsConstructor
// @AllArgsConstructor
@Slf4j
public class TestController {
  private final ProcessService processService;

  @GetMapping("/completeOrder/{id}")
  public String completeOrder(@PathVariable String id, @RequestParam Boolean confirm) {

    return processService.completeOrder(id, confirm);
  }

  // start order with rule path form DB
  @PostMapping("/startOrderWithRule")
  public String startOrderWithRule(@RequestBody OrderDetailDTO.startOrderDTO startOrderDTO) {

    return processService.startOrderProcess(startOrderDTO);
  }
}
