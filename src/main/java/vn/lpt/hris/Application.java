package vn.lpt.hris;

import co.elastic.apm.attach.ElasticApmAttacher;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
  import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import vn.lpt.hris.domains.FileStorageProperties;


@SpringBootApplication
@EnableWebSecurity
@EnableCaching
@EnableFeignClients
@EnableConfigurationProperties({
        FileStorageProperties.class
})
@ConfigurationPropertiesScan
public class Application {

  public static void main(String[] args) {
    ElasticApmAttacher.attach();
      SpringApplication.run(Application.class, args);

  }

}
