package vn.lpt.hris.domains;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Class UserTaskConfig
 *
 * @author KhanhNM
 * @since 13/02/2022
 */
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
@Setter
@Builder
@Table(name = "resign_process")
public class ResignProcess {
    @Id
    @Column(name = "id", nullable = false)
    private String id;
    private String processInstanceId;
    private String assignee;
    private String owner;
    private String ownerName;
    private Date createdDate;
    private String createdBy;
    private String updatedBy;
    private Date updatedDate;
    private String status;
    private String reason;
    private String taskId;
    private  String processDefinitionId;
}
