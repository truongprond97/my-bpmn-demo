package vn.lpt.hris.domains;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;


@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class ExcelRuleFile {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    private Date createAt;
    private int version;

    public ExcelRuleFile(String name, Date createAt) {
        this.name = name;
        this.createAt = createAt;
    }

}
