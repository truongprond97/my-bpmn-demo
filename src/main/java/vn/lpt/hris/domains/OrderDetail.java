package vn.lpt.hris.domains;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Table(name ="ORDER_DETAIL")
public class OrderDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String customerName;
    private String customerPhone;
    private int customerAge;
    private int quantity;
    private double totalPrice;
    private Boolean status;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "milk_tea_id", nullable=false)
    private MilkTea milkTea;
    @Column(insertable = false, updatable = false)
    private int milk_tea_id;

    public OrderDetail(String customerName, String customerPhone, int customerAge, int quantity, double totalPrice, boolean status, MilkTea milkTea, int milk_tea_id) {
        this.customerName = customerName;
        this.customerPhone = customerPhone;
        this.customerAge = customerAge;
        this.quantity = quantity;
        this.totalPrice = totalPrice;
        this.status = status;
        this.milkTea = milkTea;
        this.milk_tea_id = milk_tea_id;
    }


}
