package vn.lpt.hris.domains;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class ProcessRulePath {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String path;
    private String processKey;

    public ProcessRulePath(String path, String processKey) {
        this.path = path;
        this.processKey = processKey;
    }
}
