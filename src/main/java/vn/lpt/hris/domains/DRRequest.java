package vn.lpt.hris.domains;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

//@AllArgsConstructor
//@Getter
//@Setter
public class DRRequest {
    private int milkTeaId;
    private int quantity;
    private double discountPrice;
    private int gate;

    public DRRequest(int milkTeaId, int quantity) {
        this.milkTeaId = milkTeaId;
        this.quantity = quantity;
    }

    public int getMilkTeaId() {
        return milkTeaId;
    }

    public void setMilkTeaId(int milkTeaId) {
        this.milkTeaId = milkTeaId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(double discountPrice) {
        this.discountPrice = discountPrice;
    }

    public int getGate() {
        return gate;
    }

    public void setGate(int gate) {
        this.gate = gate;
    }
}
