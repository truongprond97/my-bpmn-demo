package vn.lpt.hris.domains;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.lpt.hris.models.usertaskconfig.UserTaskCreateDTO;

/**
 * Class UserTaskConfig
 *
 * @author KhanhNM
 * @since 13/02/2022
 */
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
@Setter
@Builder
@Table(name = "user_task_config")
public class UserTaskConfig {

  @Id
  @SequenceGenerator(
      name = "user_task_config_id_sequence",
      sequenceName = "user_task_config_id_sequence",
      allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_task_config_id_sequence")
  @Column(name = "id", nullable = false)
  private Long id;
  @Column(name = "user_task_id")
  private String userTaskId;
  @Column(name = "name")
  private String name;
  @Column(name = "config")
  private String config;
  @Column(name = "order_num")
  private Integer orderNum;

  private String priority;

  @ManyToOne
  @JoinColumn(name = "process_id", referencedColumnName = "process_id")
  private ProcessConfig processConfig;

  public UserTaskConfig(UserTaskCreateDTO dto) {
    this.userTaskId = dto.getUserTaskId();
    this.name = dto.getName();
    this.config = dto.getConfig();
    this.orderNum = dto.getOrderNum();
    this.priority = dto.getPriority();
  }
}