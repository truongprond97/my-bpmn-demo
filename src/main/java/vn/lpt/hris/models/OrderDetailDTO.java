package vn.lpt.hris.models;

import lombok.*;
import vn.lpt.hris.domains.MilkTea;
import vn.lpt.hris.domains.OrderDetail;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetailDTO {

    private int id;
    private String customerName;
    private String customerPhone;
    private int customerAge;
    private int quantity;
    private double totalPrice;
    private int milkTeaId;
    private Boolean status;

    public OrderDetailDTO(OrderDetail orderDetail) {
        this.id = orderDetail.getId();
        this.customerName = orderDetail.getCustomerName();
        this.customerPhone = orderDetail.getCustomerPhone();
        this.customerAge = orderDetail.getCustomerAge();
        this.quantity = orderDetail.getQuantity();
        this.totalPrice = orderDetail.getTotalPrice();
        this.milkTeaId = orderDetail.getMilk_tea_id();
//        this.milkTeaId = orderDetail.getMilkTea().getId();
        this.status = orderDetail.getStatus();
    }

    @Data
    public static class startOrderDTO{

        private String customerName;
        private String customerPhone;
        private int customerAge;
        @NotBlank
        private int quantity;
        private double totalPrice;
        @NotBlank
        private int milkTeaId;
    }


}
