package vn.lpt.hris.models;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class LeaveOfAbsenceApproveDTO {

    private String approver;
    private String approverNote;
    private String decision;
    private boolean isFinal;
}
