package vn.lpt.hris.models;

import java.time.LocalDate;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class LeaveOfAbsenceDTO  {

    private Long id;
    private String employeeCode;
    private String employeeName;
    private Object employeeDTO;
    private LocalDate startDate;
    private LocalDate endDate;
    private String reason;
    private String approverFinal;
    private String approverNoteFinal;
    private String approvedDecisionFinal;
    private LocalDate approvedDateFinal;
    private List<Object> approverDTOS;
    private String status;
}
