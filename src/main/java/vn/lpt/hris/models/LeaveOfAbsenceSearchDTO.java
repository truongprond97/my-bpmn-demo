package vn.lpt.hris.models;

import lombok.*;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class LeaveOfAbsenceSearchDTO {
    private String employeeCode;
    private LocalDate startDate;
    private LocalDate endDate;
    private String status;
    private String approverFinal;
    private LocalDate startApprovedDateFinal;
    private LocalDate endApprovedDateFinal;
}
