package vn.lpt.hris.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.lpt.hris.domains.MilkTea;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MilkTeaDTO {

    private String name;
    private Double orginPrice;

    public MilkTeaDTO(MilkTea milkTea){

        this.name=milkTea.getName();
        this.orginPrice = milkTea.getOrginPrice();
    }
}
