package vn.lpt.hris.models;

import lombok.*;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class ApprovalInformationDTO implements Serializable {

    private String employeeCode;
    private String employeeName;
    private String titleName;
    private String jobName;
    private String unitName;
    private String unitCode;
    private int index;

}
