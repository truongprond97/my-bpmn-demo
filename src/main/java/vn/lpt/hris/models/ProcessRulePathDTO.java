package vn.lpt.hris.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import vn.lpt.hris.domains.ProcessRulePath;

@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
public class ProcessRulePathDTO {
    private int id;

    private String path;
    private String processKey;

    public ProcessRulePathDTO(ProcessRulePath processRulePath) {
        this.id = processRulePath.getId();
        this.path = processRulePath.getPath();
        this.processKey = processRulePath.getProcessKey();
    }
}
