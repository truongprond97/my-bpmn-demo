package vn.lpt.hris.models.processconfig;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.lpt.hris.models.usertaskconfig.UserTaskCreateDTO;

/**
 * Class map dữ liệu đầu ra
 *
 * @author KhanhNM
 * @since 13/02/2022
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class ProcessCreateDTO {

  private String processId;
  private String config;
  private String name;
  private List<UserTaskCreateDTO> userTasks;
}