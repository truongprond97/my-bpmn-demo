package vn.lpt.hris.models.processconfig;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class map dữ liệu đầu ra
 *
 * @author KhanhNM
 * @since 13/02/2022
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class ProcessSearchDTO {

  private String processId;
  private String config;
}