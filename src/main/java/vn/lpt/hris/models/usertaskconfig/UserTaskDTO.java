package vn.lpt.hris.models.usertaskconfig;

import lombok.*;
import vn.lpt.hris.domains.UserTaskConfig;

/**
 * Class map dữ liệu đầu ra
 *
 * @author KhanhNM
 * @since 13/02/2022
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class UserTaskDTO {

    private Long id;
    private String userTaskId;
    private String name;
    private String config;
    private Integer orderNum;
    private String priority;


    public UserTaskDTO(UserTaskConfig dto) {
        this.id = dto.getId();
        this.userTaskId = dto.getUserTaskId();
        this.name = dto.getName();
        this.config = dto.getConfig();
        this.orderNum = dto.getOrderNum();
        this.priority = dto.getPriority();
//    Gson gson = new Gson();
//    Type stringMapType = new TypeToken<Map<String, Set<String>>>() {
//    }.getType();
//    Type stringListType = new TypeToken<Map<String, Set<String>>>() {
//    }.getType();
//    this.candidateUsers = gson.fromJson(dto.getCandidateUsers(), stringListType);
//    this.candidateGroups = gson.fromJson(dto.getCandidateGroups(), stringListType);
//    this.customUserIdentityLinks = gson.fromJson(dto.getCustomUserIdentityLinks(), stringMapType);
//    this.customGroupIdentityLinks = gson.fromJson(dto.getCustomGroupIdentityLinks(), stringMapType);
    }
}