package vn.lpt.hris.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NoteModel {
  private String title;
  private int index;
  private Boolean isDone;
}
