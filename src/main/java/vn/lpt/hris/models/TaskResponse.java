package vn.lpt.hris.models;

import lombok.*;

import java.util.Date;
import java.util.Map;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TaskResponse {
  private String id;
  private String name;
  private Integer appVersion;
  private String assignee;
  private String businessKey;
  private String category;
  private Date claimTime;
  private String delegationState;
  private String description;
  private Date dueDate;
  private String excutionId;
  private String parentTaskId;
  private Map<String,Object> processVariable;
  private Map<String,Object> taskLocalVariables;
  private String tenantId;
  private String owner;
  private String processDefinitionId;
  private String processInstanceId;
  private String formKey;
  private String status;
  private String type;
  private Map<String,Object> variables;
  private Date createDate;
}
