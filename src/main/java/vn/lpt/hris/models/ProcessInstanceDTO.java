package vn.lpt.hris.models;

import lombok.Data;
import org.activiti.api.process.model.ProcessInstance.ProcessInstanceStatus;

import java.util.Date;
import java.util.Map;

@Data
public class ProcessInstanceDTO {
  private String id;
  private String name;
  private Date startDate;
  private String initiator;
  private String businessKey;
  private ProcessInstanceStatus status;
  private String processDefinitionId;
  private String processDefinitionKey;
  private String parentId;
  private Integer processDefinitionVersion;
  private String processDefinitionName;

  private Map<String, Object> processVariables;

  public static ProcessInstanceDTO from(
      org.activiti.engine.runtime.ProcessInstance internalInstance) {
    ProcessInstanceDTO instance = new ProcessInstanceDTO();
    instance.id = internalInstance.getId();
    instance.parentId = internalInstance.getParentProcessInstanceId();
    instance.name = internalInstance.getName();
    instance.processDefinitionId = internalInstance.getProcessDefinitionId();
    instance.processDefinitionKey = internalInstance.getProcessDefinitionKey();
    instance.processDefinitionVersion = internalInstance.getProcessDefinitionVersion();
    instance.initiator = internalInstance.getStartUserId();
    instance.startDate = internalInstance.getStartTime();
    instance.processDefinitionKey = internalInstance.getProcessDefinitionKey();
    instance.businessKey = internalInstance.getBusinessKey();
    instance.status = calculateStatus(internalInstance);
    instance.processDefinitionVersion = internalInstance.getProcessDefinitionVersion();
    instance.processDefinitionName = internalInstance.getProcessDefinitionName();

    instance.processVariables = internalInstance.getProcessVariables();

    return instance;
  }

  private static ProcessInstanceStatus calculateStatus(
      org.activiti.engine.runtime.ProcessInstance internalProcessInstance) {
    if (internalProcessInstance.isSuspended()) {
      return ProcessInstanceStatus.SUSPENDED;
    } else if (internalProcessInstance.isEnded()) {
      return ProcessInstanceStatus.COMPLETED;
    } else if (internalProcessInstance.getStartTime() == null) {
      return ProcessInstanceStatus.CREATED;
    }
    return ProcessInstanceStatus.RUNNING;
  }
}
