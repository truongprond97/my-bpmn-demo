package vn.lpt.hris.models;

import lombok.*;

import java.io.Serializable;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class TaskCrmDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private Object leaveOfAbsenceDTO;
    private Map<String, Object> taskVariable;
    private Map<String, Object> processVariable;
    private String assignee;
    private String owner;
    private String taskName;
    private String startDate;
    private String endDate;
    private String reason;
    private String name;

}
