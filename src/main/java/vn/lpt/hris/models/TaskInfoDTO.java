package vn.lpt.hris.models;

import lombok.Data;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskInfo;

import java.util.Date;
import java.util.Map;

@Data
public class TaskInfoDTO implements TaskInfo {
  private final String id;
  private final String name;
  private final String description;
  private final int priority;
  private final String owner;
  private final String assignee;
  private final String processInstanceId;
  private final String executionId;
  private final String processDefinitionId;
  private final Date createTime;
  private final String taskDefinitionKey;
  private final Date dueDate;
  private final String category;
  private final String parentTaskId;
  private final String tenantId;
  private final String formKey;
  private final Map<String, Object> taskLocalVariables;
  private final Map<String, Object> processVariables;
  private final Date claimTime;
  private final String businessKey;

  public TaskInfoDTO(Task task) {
    this.id = task.getId();
    this.name = task.getName();
    this.description = task.getDescription();
    this.priority = task.getPriority();
    this.owner = task.getOwner();
    this.assignee = task.getAssignee();
    this.processInstanceId = task.getProcessInstanceId();
    this.executionId = task.getExecutionId();
    this.processDefinitionId = task.getProcessDefinitionId();
    this.createTime = task.getCreateTime();
    this.taskDefinitionKey = task.getTaskDefinitionKey();
    this.dueDate = task.getDueDate();
    this.category = task.getCategory();
    this.parentTaskId = task.getParentTaskId();
    this.tenantId = task.getTenantId();
    this.formKey = task.getFormKey();
    this.taskLocalVariables = task.getTaskLocalVariables();
    this.processVariables = task.getProcessVariables();
    this.claimTime = task.getClaimTime();
    this.businessKey = task.getBusinessKey();
  }
}
