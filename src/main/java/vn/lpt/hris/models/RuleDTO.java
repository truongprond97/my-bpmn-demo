package vn.lpt.hris.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RuleDTO {
    private List<ConditionDTO> conditions;
    private RuleDTO.eventType eventType;

    public List<ConditionDTO> getConditions() {
        return conditions;
    }

    public void setConditions(List<ConditionDTO> conditions) {
        this.conditions = conditions;
    }

    public RuleDTO.eventType getEventType() {
        return eventType;
    }

    public void setEventType(RuleDTO.eventType eventType) {
        this.eventType = eventType;
    }

    @Override
    public String toString(){
        StringBuilder statementBuilder = new StringBuilder();

        for (ConditionDTO condition : getConditions()) {

            String operator = null;

            switch (condition.getOperator()) {
                case EQUAL_TO:
                    operator = "==";
                    break;
                case NOT_EQUAL_TO:
                    operator = "!=";
                    break;
                case GREATER_THAN:
                    operator = ">";
                    break;
                case LESS_THAN:
                    operator = "<";
                    break;
                case GREATER_THAN_OR_EQUAL_TO:
                    operator = ">=";
                    break;
                case LESS_THAN_OR_EQUAL_TO:
                    operator = "<=";
                    break;
            }

            statementBuilder.append(condition.getField()).append(" ").append(operator).append(" ");

            if (condition.getValue() instanceof String) {
                statementBuilder.append("'").append(condition.getValue()).append("'");
            } else {
                statementBuilder.append(condition.getValue());
            }

            statementBuilder.append(" && ");
        }

        String statement = statementBuilder.toString();

        // remove trailing &&
        return statement.substring(0, statement.length() - 4);
    }

    public static enum eventType {

        ORDER("ORDER"),
        INVOICE("INVOICE");
        private final String value;
        private static Map<String, RuleDTO.eventType> constants = new HashMap<String, RuleDTO.eventType>();

        static {
            for (RuleDTO.eventType c: values()) {
                constants.put(c.value, c);
            }
        }

        private eventType(String value) {
            this.value = value;
        }

        public static RuleDTO.eventType fromValue(String value) {
            RuleDTO.eventType constant = constants.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }
}
