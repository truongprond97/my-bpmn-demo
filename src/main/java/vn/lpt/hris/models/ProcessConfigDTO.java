package vn.lpt.hris.models;

import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class ProcessConfigDTO {
    private String processId;
    private String config;
    private String userTaskId;
    private List<ProcessConfigDTO> userTasks;

}
