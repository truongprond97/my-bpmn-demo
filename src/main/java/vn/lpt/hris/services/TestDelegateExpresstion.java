package vn.lpt.hris.services;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

@Service("TestDelegateExpresstion")
public class TestDelegateExpresstion implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) {
        System.out.println("task1" + delegateExecution.getVariable("bool"));
    }
}
