package vn.lpt.hris.services;

import java.util.List;
import vn.lpt.hris.exceptions.BpmException;
import vn.lpt.hris.models.usertaskconfig.UserTaskCreateDTO;
import vn.lpt.hris.models.usertaskconfig.UserTaskDTO;

public interface UserTaskConfigService {

  /**
   * Save UserTaskConfig
   *
   * @author KhanhNM
   * @since 13/02/2022
   */
  void update(UserTaskCreateDTO dto, Long id) throws BpmException;

  /**
   * Find By ProcessId
   *
   * @author KhanhNM
   * @since 13/02/2022
   */
  List<UserTaskDTO> findByProcessId(String processId) throws BpmException;
  List<UserTaskDTO> stepByTask(String taskId, String processId);

}