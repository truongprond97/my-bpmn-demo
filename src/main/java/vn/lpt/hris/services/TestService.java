package vn.lpt.hris.services;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;

public class TestService implements JavaDelegate {



    @Override
    public void execute(DelegateExecution delegateExecution) {
        System.out.print("task1 " + delegateExecution.getVariable("bool"));
    }
}
