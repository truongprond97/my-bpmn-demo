package vn.lpt.hris.services.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.springframework.stereotype.Service;
import vn.lpt.hris.domains.UserTaskConfig;
import vn.lpt.hris.exceptions.BpmException;
import vn.lpt.hris.exceptions.ExceptionUtils;
import vn.lpt.hris.models.usertaskconfig.UserTaskCreateDTO;
import vn.lpt.hris.models.usertaskconfig.UserTaskDTO;
import vn.lpt.hris.repositories.ProcessConfigRepository;
import vn.lpt.hris.repositories.UserTaskConfigRepository;
import vn.lpt.hris.services.ProcessService;
import vn.lpt.hris.services.UserTaskConfigService;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserTaskServiceImpl implements UserTaskConfigService {

    private final ProcessConfigRepository processConfigRepository;
    private final UserTaskConfigRepository userTaskConfigRepository;
    private final ProcessService processService;
    private final TaskService taskService;

    /**
     * Save UserTaskConfig
     *
     * @param dto
     * @author KhanhNM
     * @since 13/02/2022
     */
    @Override
    public void update(UserTaskCreateDTO dto, Long id) throws BpmException {
        // Kiểm tra tồn tại process
        Optional<UserTaskConfig> optional = userTaskConfigRepository.findById(id);
        if (optional.isEmpty()) {
            throw new BpmException(ExceptionUtils.E_COMMON_NOT_EXISTS_CODE,
                    String.format(ExceptionUtils.messages.get(ExceptionUtils.E_COMMON_NOT_EXISTS_CODE), id));
        }
        UserTaskConfig userTaskConfig = optional.get();
        userTaskConfig.setConfig(dto.getConfig());
        userTaskConfigRepository.save(userTaskConfig);
    }

    /**
     * Find By ProcessId
     *
     * @param processId
     * @author KhanhNM
     * @since 13/02/2022
     */
    @Override
    public List<UserTaskDTO> findByProcessId(String processId) throws BpmException {
        return userTaskConfigRepository.findAllByProcessConfig_ProcessIdOrderByIdAsc(processId).stream()
                .map(UserTaskDTO::new)
                .sorted(Comparator.comparing(UserTaskDTO::getOrderNum))
                .collect(
                        Collectors.toList());
    }

    @Override
    public List<UserTaskDTO> stepByTask(String taskId, String processId) {
        Task task = taskService.createTaskQuery().taskId(taskId).active().singleResult();
        if (task == null) return null;
        return userTaskConfigRepository.findAllByProcessConfig_ProcessIdAndPriority(processId, String.valueOf(task.getPriority())).stream().map(UserTaskDTO::new).collect(Collectors.toList());
    }
}