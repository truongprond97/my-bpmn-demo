package vn.lpt.hris.services.impl;

import org.springframework.stereotype.Service;
import vn.lpt.hris.domains.ProcessRulePath;
import vn.lpt.hris.models.ProcessRulePathDTO;
import vn.lpt.hris.repositories.ProcessRulePathRepository;
import vn.lpt.hris.services.ProcessRulePathService;

import java.util.Optional;

@Service
public class ProcessRulePathServiceImpl implements ProcessRulePathService {
    private final ProcessRulePathRepository processRulePathRepository;

    public ProcessRulePathServiceImpl(ProcessRulePathRepository processRulePathRepository) {
        this.processRulePathRepository = processRulePathRepository;
    }

    @Override
    public ProcessRulePathDTO getByProcessKey(String processKey ) {
       Optional<ProcessRulePath> processRulePath = processRulePathRepository.findByProcessKey(processKey);
        return processRulePath.map(ProcessRulePathDTO::new).orElse(null);
        //       return new ProcessRulePathDTO(processRulePath.get().getId(),processRulePath.get().getPath(),processRulePath.get().getProcessKey());
    }
}
