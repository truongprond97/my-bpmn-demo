package vn.lpt.hris.services.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.activiti.api.runtime.shared.query.Page;
import org.activiti.api.runtime.shared.query.Pageable;
import org.activiti.api.task.model.payloads.CompleteTaskPayload;
import org.activiti.api.task.model.payloads.GetTasksPayload;
import org.activiti.api.task.runtime.TaskRuntime;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import vn.lpt.hris.common.Constants;
import vn.lpt.hris.feign.HrisServicesClient;
import vn.lpt.hris.models.TaskCrmDTO;
import vn.lpt.hris.models.TaskInfoDTO;
import vn.lpt.hris.models.UserDTO;
import vn.lpt.hris.services.TaskService;
import vn.lpt.hris.utils.Utils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class TaskServiceImpl implements TaskService {


    private final TaskRuntime taskRuntime;
    private final org.activiti.engine.TaskService taskService;
    private final RuntimeService runtimeService;
    private final HrisServicesClient hrisServicesClient;

    private static void includeTypesFilter(@NonNull TaskQuery query, String[] types) {
        if (ArrayUtils.isEmpty(types)) {
            return;
        }
        query.or();
        Arrays.stream(types).forEach(item -> query.taskVariableValueEquals(Constants.TYPE, item));
        query.endOr();
    }

    private static void includeVariables(
            @NonNull TaskQuery query,
            boolean includeTaskLocalVariables,
            boolean includeProcessVariables) {
        if (includeProcessVariables) {
            query.includeProcessVariables();
        }
        if (includeTaskLocalVariables) {
            query.includeTaskLocalVariables();
        }
    }

    @Override
    public org.activiti.api.task.model.Task complete(CompleteTaskPayload payload) {
        return taskRuntime.complete(payload);
    }

    @Override
    public Page<org.activiti.api.task.model.Task> getTasks(
            Pageable pageable, GetTasksPayload payload) {
        taskRuntime.tasks(pageable);
        return taskRuntime.tasks(pageable, payload);
    }

    @Override
    public List<Task> getTasks(
            String[] types, boolean includeTaskLocalVariables, boolean includeProcessVariables) {
        TaskQuery query = taskService.createTaskQuery();
        includeTypesFilter(query, types);
        includeVariables(query, includeTaskLocalVariables, includeProcessVariables);
        return query.orderByDueDateNullsLast().desc().list();
    }

    @Override
    public org.springframework.data.domain.Page<TaskInfoDTO> getTasksPaging(
            Integer offset,
            Integer limit,
            boolean includeTaskLocalVariables,
            boolean includeProcessVariables) {
        TaskQuery query = taskService.createTaskQuery();
        includeVariables(query, includeTaskLocalVariables, includeProcessVariables);
        List<TaskInfoDTO> tasks =
                query.listPage(offset, limit).stream().map(TaskInfoDTO::new).collect(Collectors.toList());
        org.springframework.data.domain.Pageable pageable = PageRequest.of(offset, limit);
        return new org.springframework.data.domain.PageImpl<>(tasks, pageable, query.count());
    }

    @Override
    public TaskCrmDTO getTaskById(
            boolean includeTaskLocalVariables, boolean includeProcessVariables, String taskId)
            throws ClassNotFoundException {
        UserDTO user = Utils.getUser();
        String token = String.format("Bearer %s",user.getToken());
        TaskQuery query = taskService.createTaskQuery().taskId(taskId);
        includeVariables(query, includeTaskLocalVariables, includeProcessVariables);
        Task task = query.singleResult();
        if (task == null) return TaskCrmDTO.builder().build();
        Map<String, Object> processVariables = task.getProcessVariables();
        String startDate = (String)processVariables.get("startDateVariable");
        String endDate = (String)processVariables.get("endDateVariable");
        String reason = (String)processVariables.get("reason");
        Long leaveAbsenceId = (Long) processVariables.get("leaveAbsenceId");
        Object search = hrisServicesClient.search(token,leaveAbsenceId);
        return TaskCrmDTO.builder().name(task.getName()).startDate(startDate).endDate(endDate).reason(reason).assignee(task.getAssignee()).owner(task.getOwner().toUpperCase()).processVariable(task.getProcessVariables()).taskVariable(task.getTaskLocalVariables()).id(taskId).leaveOfAbsenceDTO(search).build();
    }

    @Override
    public Task getCurrentTask(
            boolean includeTaskLocalVariables,
            boolean includeProcessVariables,
            String processInstanceId) {
        TaskQuery query = taskService.createTaskQuery().processInstanceId(processInstanceId);
        includeVariables(query, includeTaskLocalVariables, includeProcessVariables);
        return query.singleResult();
    }
}
