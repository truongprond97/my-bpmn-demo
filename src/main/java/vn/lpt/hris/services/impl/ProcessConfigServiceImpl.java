package vn.lpt.hris.services.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.Process;
import org.activiti.bpmn.model.ValuedDataObject;
import org.activiti.engine.repository.ProcessDefinition;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import vn.lpt.hris.domains.ProcessConfig;
import vn.lpt.hris.domains.UserTaskConfig;
import vn.lpt.hris.exceptions.BpmException;
import vn.lpt.hris.exceptions.ExceptionUtils;
import vn.lpt.hris.models.ProcessConfigDTO;
import vn.lpt.hris.models.processconfig.ProcessCreateDTO;
import vn.lpt.hris.models.processconfig.ProcessDTO;
import vn.lpt.hris.models.usertaskconfig.UserTaskCreateDTO;
import vn.lpt.hris.repositories.ProcessConfigRepository;
import vn.lpt.hris.repositories.UserTaskConfigRepository;
import vn.lpt.hris.services.ProcessConfigService;
import vn.lpt.hris.services.ProcessService;
import vn.lpt.hris.utils.BpmUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProcessConfigServiceImpl implements ProcessConfigService {

    private final ProcessConfigRepository processConfigRepository;
    private final UserTaskConfigRepository userTaskConfigRepository;
    private final ProcessService processService;
    private final ObjectMapper objectMapper;

    /**
     * Save ProcessConfig
     *
     * @author KhanhNM
     * @since 13/02/2022
     */
    @Override
    public void update(ProcessCreateDTO dto, Long id) throws BpmException {
        // Kiểm tra tồn tại process
        Optional<ProcessConfig> optional = processConfigRepository.findById(id);
        ProcessDefinition processDefinition = processService.getDefinition(dto.getProcessId());
        ProcessConfig processConfig;
        List<UserTaskConfig> userTaskConfigs = new ArrayList<>();
        if (optional.isEmpty()) {
            processConfig = new ProcessConfig();
            // Lấy danh sách UserTask
            BpmnModel bpmnModel = processService.getbpmn(dto.getProcessId());
            userTaskConfigs = BpmUtils.getUserTask(bpmnModel)
                    .stream()
                    .map(UserTaskCreateDTO::new)
                    .map(UserTaskConfig::new)
                    .collect(
                            Collectors.toList());
            processConfig.setProcessId(dto.getProcessId());
            processConfig.setName(processDefinition.getName());
        } else {
            processConfig = optional.get();
        }
        processConfig.setConfig(dto.getConfig());
        processConfigRepository.save(processConfig);

        if (!CollectionUtils.isEmpty(userTaskConfigs)) {
            userTaskConfigs.forEach(userTaskConfig -> userTaskConfig.setProcessConfig(processConfig));
            userTaskConfigRepository.saveAll(userTaskConfigs);
        }
    }

    /**
     * Find By ProcessId
     *
     * @author KhanhNM
     * @since 13/02/2022
     */
    @Override
    public ProcessDTO findByProcessId(String processId)
            throws BpmException, JsonProcessingException {
        // Kiểm tra tồn trại process
        Optional<ProcessConfig> optional = processConfigRepository.findProcessConfigByProcessId(
                processId);
        // Nếu không tồn tại, lấy process theo bpmn
        if (optional.isEmpty()) {
            ProcessDefinition processDefinition = processService.getDefinition(
                    processId);
            if (processDefinition == null) {
                throw new BpmException(ExceptionUtils.E_COMMON_NOT_EXISTS_CODE,
                        String.format(ExceptionUtils.messages.get(ExceptionUtils.E_COMMON_NOT_EXISTS_CODE),
                                processId));
            }
            ProcessConfig processConfig = new ProcessConfig();
            // Lấy userTask theo bpmn
            BpmnModel bpmnModel = processService.getbpmn(processId);
            if (!CollectionUtils.isEmpty(bpmnModel.getProcesses())) {
                Process process = bpmnModel.getProcesses().get(0);
                List<Object> statusList = process.getDataObjects().stream().map(ValuedDataObject::getValue).collect(Collectors.toList());
                processConfig.setStatus(objectMapper.writeValueAsString(statusList));
            }
            List<UserTaskConfig> userTasks = BpmUtils.getUserTask(bpmnModel)
                    .stream()
                    .map(UserTaskCreateDTO::new)
                    .map(UserTaskConfig::new).collect(
                            Collectors.toList());
            processConfig.setProcessId(processId);
            processConfig.setName(processDefinition.getName());
            userTasks.forEach(userTaskConfig -> userTaskConfig.setProcessConfig(processConfig));
            processConfig.setUserTasks(new HashSet<>(userTasks));
            // Nếu có trong bpmn mà không có trong data base thì lưu lại trong database
            processConfigRepository.save(processConfig);
            userTaskConfigRepository.saveAll(userTasks);
            return new ProcessDTO(processConfig, objectMapper);
        }
        return new ProcessDTO(optional.get(), objectMapper);
    }

    @Override
    public void updateProcessConfig(ProcessConfigDTO dto) throws BpmException {
        // Kiểm tra tồn trại process
        Optional<ProcessConfig> processConfigOptional = processConfigRepository.findProcessConfigByProcessId(
                dto.getProcessId());
        if (processConfigOptional.isEmpty())
            throw new BpmException(ExceptionUtils.E_COMMON_NOT_EXISTS_CODE,
                    String.format(ExceptionUtils.messages.get(ExceptionUtils.E_COMMON_NOT_EXISTS_CODE),
                            dto.getProcessId()));
        ProcessConfig processConfig = processConfigOptional.get();
        processConfig.setConfig(dto.getConfig());
        processConfigRepository.save(processConfig);

    }


    @Override
    public void updateStepConfig(ProcessConfigDTO dto) throws BpmException {
        // Kiểm tra tồn trại process
        Optional<ProcessConfig> processConfigOptional = processConfigRepository.findProcessConfigByProcessId(
                dto.getProcessId());
        if (processConfigOptional.isEmpty())
            throw new BpmException(ExceptionUtils.E_COMMON_NOT_EXISTS_CODE,
                    String.format(ExceptionUtils.messages.get(ExceptionUtils.E_COMMON_NOT_EXISTS_CODE),
                            dto.getProcessId()));
        if (CollectionUtils.isEmpty(dto.getUserTasks()))
            return;
        List<String> userTaskId = dto.getUserTasks().stream().map(ProcessConfigDTO::getUserTaskId).collect(Collectors.toList());
        Map<String, String> mapConfig = dto.getUserTasks().stream().collect(Collectors.toMap(ProcessConfigDTO::getUserTaskId, ProcessConfigDTO::getConfig));
        List<UserTaskConfig> userTaskConfigList = userTaskConfigRepository.findAllByUserTaskIdInAndProcessConfig_ProcessId(userTaskId, dto.getProcessId());
        userTaskConfigList.forEach(i -> i.setConfig(mapConfig.get(i.getUserTaskId())));
        userTaskConfigRepository.saveAll(userTaskConfigList);
    }
}