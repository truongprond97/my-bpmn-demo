package vn.lpt.hris.services;

import org.drools.decisiontable.InputType;
import org.drools.decisiontable.SpreadsheetCompiler;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieModule;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.StatelessKieSession;
import org.kie.internal.io.ResourceFactory;
import org.kie.internal.utils.KieHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import vn.lpt.hris.config.DroolsConfiguration;
import vn.lpt.hris.domains.ExcelRuleFile;
import vn.lpt.hris.domains.ReloadDroolsRules;
import vn.lpt.hris.exceptions.FileStorageException;
import vn.lpt.hris.exceptions.MyFileNotFoundException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;

@Service
public class FileStorageService {
    private final Path fileStorageLocation;
    private ExcelRuleFileService excelRuleFileService;
    private final KieContainer kieContainer;

    @Autowired
    public FileStorageService(Environment env, ExcelRuleFileService excelRuleFileService, KieContainer kieContainer) {
        this.fileStorageLocation = Paths.get(env.getProperty("app.file.upload-dir",
                "./src/main/resources/rules"
        ))
                .toAbsolutePath().normalize();
        this.excelRuleFileService = excelRuleFileService;
        this.kieContainer = kieContainer;

        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new RuntimeException(
                    "Could not create the directory where the uploaded files will be stored.", ex);
        }
    }

//    @Autowired
//    public FileStorageService(FileStorageProperties fileStorageProperties) {
//        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
//                .toAbsolutePath().normalize();
//
//        try {
//            Files.createDirectories(this.fileStorageLocation);
//        } catch (Exception ex) {
//            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
//        }
//    }

    public String storeFile(MultipartFile file) {
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            excelRuleFileService.save(new ExcelRuleFile(fileName,new Date()));

            KieServices kieServices = KieServices.Factory.get();
            KieFileSystem kieFileSystem = kieServices.newKieFileSystem();
//      File file1 = new File(file.getOriginalFilename());
//      Resource resource = kieServices.getResources().newFileSystemResource(file1).setResourceType(
//          ResourceType.DRL);
//      kfs.write( resource);
//      KieBuilder kb = kieServices.newKieBuilder(kfs);
//      kb.buildAll();

//            kieFileSystem.write("C:/Users/Admin/OneDrive/Desktop/DiscountRule1.xls", file.getBytes());
            KieBuilder kb = kieServices.newKieBuilder(kieFileSystem);
            KieModule kieModule = kb.getKieModule();
            kieContainer.updateToVersion(kieModule.getReleaseId());
            System.out.println("Rule changed successfully!" );


//            StatelessKieSession kSession = new KieHelper().addContent(rules, ResourceType.DRL).build().newStatelessKieSession();

            return fileName;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public Resource loadFileAsResource(String fileName) {
        try {
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if(resource.exists()) {
                return resource;
            } else {
                throw new MyFileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new MyFileNotFoundException("File not found " + fileName, ex);
        }
    }
}
