package vn.lpt.hris.services;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import vn.lpt.hris.domains.ExcelRuleFile;
import vn.lpt.hris.repositories.ExcelRuleFileRepository;

import java.util.List;

@Service
public class ExcelRuleFileService {
  final ExcelRuleFileRepository excelRuleFileRepository;

  public ExcelRuleFileService(ExcelRuleFileRepository excelRuleFileRepository) {
    this.excelRuleFileRepository = excelRuleFileRepository;
  }

  public ExcelRuleFile save(ExcelRuleFile excelRuleFile) {

    if (getLatestFile() != null) {
      excelRuleFile.setVersion(getLatestFile().getVersion() + 1);
    } else {
      excelRuleFile.setVersion(1);
    }
    return excelRuleFileRepository.save(excelRuleFile);
  }

  public ExcelRuleFile getLatestFile() {
    List<ExcelRuleFile> list = excelRuleFileRepository.findByOrderByVersionDesc();
    if(list.size()==0){
      return null;
    }
    return list.get(0);
  }
}
