package vn.lpt.hris.services;

import org.springframework.stereotype.Service;
import vn.lpt.hris.domains.OrderDetail;
import vn.lpt.hris.models.OrderDetailDTO;
import vn.lpt.hris.repositories.OrderDetailRepository;

@Service
public class OrderDetailService {
    final OrderDetailRepository orderDetailRepository;

    public OrderDetailService(OrderDetailRepository orderDetailRepository) {
        this.orderDetailRepository = orderDetailRepository;
    }

    public OrderDetailDTO findById(int id){
        return new OrderDetailDTO(orderDetailRepository.getById(id));
    }

    public OrderDetailDTO save(OrderDetail orderDetail){
        return new OrderDetailDTO(orderDetailRepository.save(orderDetail));
    }

    public OrderDetailDTO update(OrderDetailDTO orderDetailDTO){
       OrderDetail orderDetail=   orderDetailRepository.getById(orderDetailDTO.getId());
       orderDetail.setStatus(true);
       return new OrderDetailDTO(orderDetailRepository.save(orderDetail));
    }
}
