package vn.lpt.hris.services;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import vn.lpt.hris.feign.HrisServicesClient;
import vn.lpt.hris.models.ApprovalInformationDTO;
import vn.lpt.hris.shared.configurations.ApplicationContextHolder;

import java.util.List;
import java.util.Map;

@Service
public class ApproverService implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) {
        HrisServicesClient hrisServicesClient = ApplicationContextHolder.getBean(HrisServicesClient.class);
        KeycloakService keycloakService = ApplicationContextHolder.getBean(KeycloakService.class);
        String auth = keycloakService.getServiceAccountAccessToken(true);
//        hrisServicesClient.findByApprove()
        Map<String, Object> variables = execution.getVariables();
        String unitCode = (String) variables.get("unitCode");
        List<ApprovalInformationDTO> byApprove = hrisServicesClient.findByApprove(auth, unitCode);
        for (int i = 0; i < byApprove.size(); i++) {
            int finalI = i + 1;
            ApprovalInformationDTO approvalInformationDTO = byApprove.stream().filter(a -> a.getIndex() == finalI).findFirst().orElse(null);
            execution.setVariable(String.format("approver%s", finalI), approvalInformationDTO != null ? approvalInformationDTO.getEmployeeCode() : StringUtils.EMPTY);
        }
    }


}
