package vn.lpt.hris.services;

import java.util.List;
import org.activiti.api.runtime.shared.query.Page;
import org.activiti.api.runtime.shared.query.Pageable;
import org.activiti.api.task.model.payloads.CompleteTaskPayload;
import org.activiti.api.task.model.payloads.GetTasksPayload;
import org.activiti.engine.task.Task;
import vn.lpt.hris.models.TaskCrmDTO;
import vn.lpt.hris.models.TaskInfoDTO;

public interface TaskService {

  /**
   * complete a task
   *
   * @param payload CompleteTaskPayload
   * @author: vanna
   */
  org.activiti.api.task.model.Task complete(CompleteTaskPayload payload);

  /**
   * list tasks
   *
   * @param pageable Pageable
   * @param payload  GetTasksPayload
   * @return page of Task
   * @author vanna
   */
  Page<org.activiti.api.task.model.Task> getTasks(Pageable pageable, GetTasksPayload payload);

  List<Task> getTasks(
      String[] types, boolean includeTaskLocalVariables, boolean includeProcessVariables)
      throws IllegalAccessException;

  /**
   * list tasks
   *
   * @return page of Task
   * @author vanna
   */
  org.springframework.data.domain.Page<TaskInfoDTO> getTasksPaging(
      Integer offset,
      Integer limit,
      boolean includeTaskLocalVariables,
      boolean includeProcessVariables)
      throws IllegalAccessException;

  TaskCrmDTO getTaskById(
      boolean includeTaskLocalVariables, boolean includeProcessVariables, String taskId)
      throws IllegalAccessException, ClassNotFoundException;

  Task getCurrentTask(
      boolean includeTaskLocalVariables, boolean includeProcessVariables, String processInstanceId)
      throws IllegalAccessException;
}
