package vn.lpt.hris.services;


import org.keycloak.admin.client.Keycloak;

public interface KeycloakService {
  Keycloak getServiceAccountKeycloak();

  String getServiceAccountAccessToken();

  String getServiceAccountAccessToken(boolean includedBearer);
}
