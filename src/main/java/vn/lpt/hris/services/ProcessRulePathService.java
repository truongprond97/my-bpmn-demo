package vn.lpt.hris.services;

import vn.lpt.hris.domains.ProcessRulePath;
import vn.lpt.hris.models.ProcessRulePathDTO;

public interface ProcessRulePathService {
    ProcessRulePathDTO getByProcessKey(String processKey);
}
