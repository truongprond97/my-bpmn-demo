package vn.lpt.hris.services;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.stereotype.Service;
import vn.lpt.hris.domains.MilkTea;
import vn.lpt.hris.domains.OrderDetail;
import vn.lpt.hris.models.OrderDetailDTO;
import vn.lpt.hris.repositories.MilkTeaRepository;
import vn.lpt.hris.repositories.OrderDetailRepository;

import java.util.Map;
import java.util.Optional;

@Service("TestExpresstion")
public class TestExpresstion {
  final MilkTeaRepository milkTeaRepository;
  final RuntimeService runtimeService;
  final TaskService taskService;
  final OrderDetailRepository orderDetailRepository;
  final OrderDetailService orderDetailService;

  public TestExpresstion(
          MilkTeaRepository milkTeaRepository,
          RuntimeService runtimeService,
          TaskService taskService,
          OrderDetailRepository orderDetailRepository, OrderDetailService orderDetailService) {
    this.milkTeaRepository = milkTeaRepository;
    this.runtimeService = runtimeService;
    this.taskService = taskService;
    this.orderDetailRepository = orderDetailRepository;
    this.orderDetailService = orderDetailService;
    {
    };
  }

  public void task1() {

    System.out.println("Task1");
  }

  public void task2(String id) {
    //        Task tasks = taskService.createTaskQuery().taskId(id).singleResult();

    System.out.println("Task2 : ");
  }

  public void caculateTotalPrice(
      int discountPrice,
      String customerName,
      String customerPhone,
      int customerAge,
      int milkTeaId,
      int quantity,
      String processInstanceId) {

    Optional<MilkTea> milkTea = milkTeaRepository.findById(milkTeaId);
    if (!milkTea.isPresent()) {
      System.out.println(" Not Save  :" + discountPrice);
    }

    double totalPrice = quantity * milkTea.get().getOrginPrice() * discountPrice / 100;
    OrderDetail orderDetail = new OrderDetail(customerName,customerPhone,customerAge,quantity,totalPrice,false,milkTea.get(),milkTeaId);
//        new OrderDetail(
//            customerName, customerPhone, customerAge, quantity, totalPrice, milkTeaId, false);
    OrderDetailDTO saveOrderDetail = orderDetailService.save(orderDetail);
    ProcessInstance processInstance =
        runtimeService
            .createProcessInstanceQuery()
            .processInstanceId(processInstanceId)
            .includeProcessVariables()
            .singleResult();
    // vcl đêm hôm nhìn nhầm processdeffine sang intance
    Map<String, Object> processVariables = processInstance.getProcessVariables();
    processVariables.put("orderId", saveOrderDetail.getId());
    runtimeService.setVariablesLocal(processInstance.getId(), processVariables);
    System.out.println("Save  " + discountPrice + "% order :" + saveOrderDetail.toString());
  }

  public void saveOrder(int orderId) {
    OrderDetailDTO orderDetail = orderDetailService.findById( orderId);
    orderDetail.setStatus(true);
    orderDetailService.update(orderDetail);
    //    orderDetailRepository.updateStatus(orderId);

    System.out.println(" luu don");
  }
}
