package vn.lpt.hris.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import vn.lpt.hris.domains.OrderDetail;
@Repository
public interface OrderDetailRepository extends JpaRepository<OrderDetail,Integer> {
    @Modifying(flushAutomatically = true)
    @Query("update OrderDetail o set o.status = true where o.id =: id")
    void updateStatus(@Param(value = "id") int  id);
}
