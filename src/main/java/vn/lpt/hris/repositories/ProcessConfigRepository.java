package vn.lpt.hris.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import vn.lpt.hris.domains.ProcessConfig;

import java.util.Optional;

public interface ProcessConfigRepository extends
        PagingAndSortingRepository<ProcessConfig, Long>,
        JpaSpecificationExecutor<ProcessConfig> {

    Optional<ProcessConfig> findProcessConfigByProcessId(String processId);

    Optional<ProcessConfig> findTopByProcessId(String processId);
}