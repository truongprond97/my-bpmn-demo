package vn.lpt.hris.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.lpt.hris.domains.ProcessRulePath;

import java.util.Optional;

public interface ProcessRulePathRepository extends JpaRepository<ProcessRulePath,Integer> {
    Optional<ProcessRulePath> findByProcessKey(String  ProcessKey);
}
