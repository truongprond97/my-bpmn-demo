package vn.lpt.hris.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import vn.lpt.hris.domains.UserTaskConfig;

import java.util.List;
import java.util.Optional;

public interface UserTaskConfigRepository extends
        PagingAndSortingRepository<UserTaskConfig, Long>,
        JpaSpecificationExecutor<UserTaskConfig> {

    List<UserTaskConfig> findAllByProcessConfig_ProcessIdOrderByIdAsc(String processId);

    Optional<UserTaskConfig> findTopByProcessConfig_ProcessIdOrderByOrderNumAsc(String processId);

    Optional<UserTaskConfig> findTopByUserTaskIdAndProcessConfig_ProcessId(String userTaskId, String processId);

    List<UserTaskConfig> findAllByUserTaskIdInAndProcessConfig_ProcessId(List<String> userTaskIds, String processId);

    List<UserTaskConfig> findAllByProcessConfig_ProcessIdAndPriority( String processId, String priority);
}