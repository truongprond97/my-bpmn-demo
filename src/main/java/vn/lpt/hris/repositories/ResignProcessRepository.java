package vn.lpt.hris.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import vn.lpt.hris.domains.ResignProcess;

import java.util.Optional;

public interface ResignProcessRepository extends
        PagingAndSortingRepository<ResignProcess, String>,
        JpaSpecificationExecutor<ResignProcess> {

    Optional<ResignProcess> findTopByProcessInstanceId(String processInstanceId);
}
