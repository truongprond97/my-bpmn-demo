package vn.lpt.hris.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.lpt.hris.domains.ExcelRuleFile;

import java.util.List;

public interface ExcelRuleFileRepository extends JpaRepository<ExcelRuleFile,Integer> {

    List<ExcelRuleFile> findByOrderByVersionDesc();
}
