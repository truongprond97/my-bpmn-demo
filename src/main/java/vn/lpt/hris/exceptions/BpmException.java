package vn.lpt.hris.exceptions;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class BpmException kế thừa Exception phục vụ việc xử lý và tổng hợp lỗi trong sourceCode Bpm.
 *
 * @author KhanhNM
 * @since 13/02/2011
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class BpmException extends Exception {

  private String messageKey;
  private String message;
  private Throwable throwable;
  private List<String> messages;

  public BpmException(String msgKey) {
    this.messageKey = msgKey;
  }

  public BpmException(String msgKey, String msg) {
    this.messageKey = msgKey;
    this.message = msg;
  }

  public String getMessage() {
    if (this.message != null) {
      return message;
    }
    if (this.messageKey != null) {
      this.message = String.format(ExceptionUtils.messages.get(this.messageKey));
    }
    return null;
  }
}