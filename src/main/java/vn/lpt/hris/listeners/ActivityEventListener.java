package vn.lpt.hris.listeners;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.delegate.event.ActivitiEntityEvent;
import org.activiti.engine.delegate.event.ActivitiEvent;
import org.activiti.engine.delegate.event.ActivitiEventListener;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.task.Task;
import vn.lpt.hris.repositories.ResignProcessRepository;
import vn.lpt.hris.utils.Utils;
/**
 * ghi log activities listerner
 *
 * @author KhanhNM
 * @since 13/02/2022
 */
@Slf4j
@RequiredArgsConstructor
public class ActivityEventListener implements ActivitiEventListener {
    private final ResignProcessRepository resignProcessRepository;

    @Override
    public void onEvent(ActivitiEvent event) {
//        String username = Utils.getUser().getUsername();
//        ActivitiEntityEvent activityEntityEvent = (ActivitiEntityEvent) event;
//        TaskEntity taskEntity = (TaskEntity) activityEntityEvent.getEntity();
//        ExecutionEntity exEntity = taskEntity.getProcessInstance();
//        Task task = taskEntity;
        switch (event.getType()) {
            case TASK_CREATED:
//                log.info("Create task - {} - username {}: ", task.getId(), username);
//                log.info("Create task type  : {} ", task.getCategory());
//                if (StringUtils.equalsAnyIgnoreCase(task.getCategory(), "ResignProcess")) {
//                    Map<String, Object> taskLocalVariables = task.getTaskLocalVariables();
//                    String assignee = (String) taskLocalVariables.get("assignee");
//                    String owner = (String) taskLocalVariables.get("owner");
//                    String ownerName = (String) taskLocalVariables.get("ownerName");
//                    String status = (String) taskLocalVariables.get("status");
//                    String resignProcessId = (String) taskLocalVariables.get("resignProcessId");
//                    if (StringUtils.isNotBlank(resignProcessId)) {
//                        resignProcessRepository.findById(resignProcessId).ifPresent(i -> {
//                            i.setStatus(status);
//                            i.setUpdatedBy(username);
//                            i.setUpdatedDate(new Date());
//                            resignProcessRepository.save(i);
//                        });
//                    } else {
//                        resignProcessRepository.save(ResignProcess.builder().owner(owner).assignee(assignee).ownerName(ownerName).status(status).createdDate(new Date()).createdBy(Utils.getUser().getUsername()).processDefinitionId(task.getProcessDefinitionId()).taskId(task.getId()).id(UUID.randomUUID().toString()).processInstanceId(task.getProcessInstanceId()).build());
//
//                    }
//                }
                break;
            case TASK_COMPLETED:
                //taskServiceWf.deleteTaskEs(task.getId());
//                log.info("Completed task: " + task.getId());
                break;
        }
    }

    @Override
    public boolean isFailOnException() {
        return false;
    }
}
