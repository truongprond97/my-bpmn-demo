package vn.lpt.hris.listeners;

import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.activiti.engine.runtime.ProcessInstance;
import vn.lpt.hris.shared.configurations.ApplicationContextHolder;

import java.util.Map;

@Slf4j
public class MyAssignmentHandler implements TaskListener {
    @Override
    public void notify(DelegateTask delegateTask) {
        Map<String, Object> processVariables = delegateTask.getExecution().getParent().getVariables();
        delegateTask.setAssignee((String)processVariables.get("approver1"));
        delegateTask.setOwner((String)processVariables.get("owner"));
    }
}
