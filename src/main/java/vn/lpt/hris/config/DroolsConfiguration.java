package vn.lpt.hris.config;

import jdk.jfr.Event;
import org.activiti.engine.impl.interceptor.Session;
import org.drools.core.io.impl.UrlResource;
import org.drools.decisiontable.InputType;
import org.drools.decisiontable.SpreadsheetCompiler;
import org.drools.template.ObjectDataCompiler;
import org.drools.template.jdbc.ResultSetGenerator;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieModule;
import org.kie.api.builder.KieRepository;
import org.kie.api.definition.rule.Rule;
import org.kie.api.io.KieResources;
import org.kie.api.io.Resource;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.StatelessKieSession;
import org.kie.internal.io.ResourceFactory;
import org.kie.internal.utils.KieHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.support.DefaultSingletonBeanRegistry;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.file.remote.session.SessionFactory;
import org.springframework.stereotype.Component;
import vn.lpt.hris.domains.DRRequest;
import vn.lpt.hris.models.ConditionDTO;
import vn.lpt.hris.models.RuleDTO;
import vn.lpt.hris.services.ExcelRuleFileService;

import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.builder.*;
import org.kie.api.runtime.KieContainer;
import org.kie.internal.io.ResourceFactory;
// import org.kie.spring.KModuleBeanFactoryPostProcessor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import vn.lpt.hris.utils.KieUtils;

import javax.persistence.EntityManagerFactory;
import java.io.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Configuration
// @ComponentScan("")
@RefreshScope
public class DroolsConfiguration {
  private final KieServices kieServices = KieServices.Factory.get();
  //    public static final String FULL_PATH = "DiscountRule.xls";
  public static String FULL_PATH = "/src/main/resources/rules/";
  public static final String RULES_PATH = "rules/";
  private final ExcelRuleFileService excelRuleFileService;

  private final EntityManagerFactory entityManagerFactory;

  public DroolsConfiguration(
      ExcelRuleFileService excelRuleFileService, EntityManagerFactory entityManagerFactory) {
    this.excelRuleFileService = excelRuleFileService;
    this.entityManagerFactory = entityManagerFactory;
  }

  //// inside classpath rule
  //    @Bean
  //    public KieContainer getKieContainer() {
  //
  //        KieFileSystem kieFileSystem = kieServices.newKieFileSystem();
  //        kieFileSystem.write(ResourceFactory.newClassPathResource(RULES_PATH+
  // excelRuleFileService.getLatestFile().getName()));
  //        KieBuilder kb = kieServices.newKieBuilder(kieFileSystem);
  //        kb.buildAll();
  //        KieModule kieModule = kb.getKieModule();
  //        return kieServices.newKieContainer(kieModule.getReleaseId());
  //    }

  // outside classpath rules
  @Bean
  public KieContainer kieContainer() {
    KieServices kieServices = KieServices.Factory.get();

    KieFileSystem kfs = kieServices.newKieFileSystem();
    KieRepository kr = kieServices.getRepository();
    File file = new File("C://Users/Admin/OneDrive/Desktop/DiscountRule2.xls");
    Resource resource = kieServices.getResources().newFileSystemResource(file);
    kfs.write(resource);

    KieBuilder kb = kieServices.newKieBuilder(kfs);
    kb.buildAll();
    return kieServices.newKieContainer(kr.getDefaultReleaseId());
  }

  @Bean
  public KieSession KieSession() {
    System.out.println("session created...");
    return kieContainer().newKieSession();
  }

  //// dyanmic rule
  //
  //    @Bean
  //    public KieContainer kieContainer(){
  //
  //        RuleDTO quantityDiscountIncRule = new RuleDTO();
  //
  //        ConditionDTO quantityDiscountCondition1 = new ConditionDTO();
  //        quantityDiscountCondition1.setField("quantity");
  //        quantityDiscountCondition1.setOperator(ConditionDTO.Operator.GREATER_THAN);
  //        quantityDiscountCondition1.setValue(1);
  //
  //        ConditionDTO quantityDiscountCondition2 = new ConditionDTO();
  //        quantityDiscountCondition2.setField("quantity");
  //        quantityDiscountCondition2.setOperator(ConditionDTO.Operator.LESS_THAN_OR_EQUAL_TO);
  //        quantityDiscountCondition2.setValue(3);
  //
  //        quantityDiscountIncRule.setEventType(RuleDTO.eventType.ORDER);
  //
  // quantityDiscountIncRule.setConditions(Arrays.asList(quantityDiscountCondition1,quantityDiscountCondition2));
  //
  //        String drl = applyRuleTemplate(orderEvent, quantityDiscountIncRule);
  //
  //        KieServices kieServices = KieServices.Factory.get();
  //
  //        KieFileSystem kfs = kieServices.newKieFileSystem();
  //        KieRepository kr = kieServices.getRepository();
  //        File file = new File("C://Users/Admin/OneDrive/Desktop/DiscountRule2.xls");
  //        Resource resource =  kieServices.getResources().newFileSystemResource(file);
  //        kfs.write(resource);
  //
  //        KieBuilder kb = kieServices.newKieBuilder(kfs);
  //        kb.buildAll();
  //        return kieServices.newKieContainer(kr.getDefaultReleaseId());
  //    }

  //
  //    static private AlertDecision evaluate(String drl, Event event) throws Exception {
  //        KieServices kieServices = KieServices.Factory.get();
  //        KieFileSystem kieFileSystem = kieServices.newKieFileSystem();
  //        kieFileSystem.write("src/main/resources/rule.drl", drl);
  //        kieServices.newKieBuilder(kieFileSystem).buildAll();
  //
  //        KieContainer kieContainer =
  // kieServices.newKieContainer(kieServices.getRepository().getDefaultReleaseId());
  //        StatelessKieSession statelessKieSession =
  // kieContainer.getKieBase().newStatelessKieSession();
  //
  //        AlertDecision alertDecision = new AlertDecision();
  //        statelessKieSession.getGlobals().set("alertDecision", alertDecision);
  //        statelessKieSession.execute(event);
  //
  //        return alertDecision;
  //    }

  //
  //    static private String applyRuleTemplate(Event event, Rule rule) throws Exception {
  //        Map<String, Object> data = new HashMap<String, Object>();
  //        ObjectDataCompiler objectDataCompiler = new ObjectDataCompiler();
  //
  //        data.put("rule", rule);
  //        data.put("eventType", event.getClass().getName());
  //
  //        return objectDataCompiler.compile(Arrays.asList(data),
  // Thread.currentThread().getContextClassLoader().getResourceAsStream("rule-template.drl"));
  //    }

}
