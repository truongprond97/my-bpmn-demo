package vn.lpt.hris.shared.configurations;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.OAuthFlow;
import io.swagger.v3.oas.models.security.OAuthFlows;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.security.SecurityScheme.Type;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.keycloak.adapters.springboot.KeycloakSpringBootProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.function.SupplierUtils;

@Configuration
@RequiredArgsConstructor
public class OpenApi30Configuration {
  private final KeycloakSpringBootProperties properties;

  @Value("${spring.application.name}")
  private String applicationName;

  @Bean
  public OpenAPI customOpenAPI() {
    var securitySchemeName = "oAuth2";
    var apiTitle = String.format("%s API", applicationName);
    return new OpenAPI()
        .addSecurityItem(new SecurityRequirement().addList(securitySchemeName))
        .components(
            new Components()
                .addSecuritySchemes(
                    securitySchemeName,
                    new SecurityScheme()
                        .name(securitySchemeName)
                        .type(Type.OAUTH2)
                        .flows(
                            SupplierUtils.resolve(
                                () ->
                                    new OAuthFlows()
                                        .authorizationCode(
                                            new OAuthFlow()
                                                .authorizationUrl(
                                                    buildUrl("/protocol/openid-connect/auth"))
                                                .tokenUrl(
                                                    buildUrl("/protocol/openid-connect/token"))
                                                .refreshUrl(
                                                    buildUrl("/protocol/openid-connect/token")))))))
        .info(new Info().title(apiTitle).version("1.0"));
  }

  private String buildUrl(String path) {
    return StringUtils.join(
        StringUtils.stripEnd(properties.getAuthServerUrl(), "/"),
        "/realms/",
        properties.getRealm(),
        path);
  }
}
